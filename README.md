# [php-packages-demo.gitlab.io](https://php-packages-demo.gitlab.io)

Unofficial demo and howto for PHP packages
* https://libraries.io/packagist

## Searching for popular packages
* [site:packagist.org](https://www.google.com/search?q=site:packagist.org)
* [site:phppackages.org](https://www.google.com/search?q=site:phppackages.org)
* [site:libraries.io/packagist](https://www.google.com/search?q=site:libraries.io/packagist)
* [site:packages.debian.org/sid/php](https://www.google.com/search?q=site:packages.debian.org/sid/php)

## Some projects not in Packagist.org
### Backdrop
* [Backdrop CMS](https://backdropcms.org/)
* [backdrop/backdrop](https://github.com/backdrop/backdrop)

## Some projects in this group by monthly download
* <!--- symfony/symfony -->
  <!--- 7 2018-10 -->
  ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/symfony)
  [symfony](https://phppackages.org/s/symfony)/[symfony](https://gitlab.com/php-packages-demo/symfony)
<!--
  [![PHPPackages Rank](http://phppackages.org/p/symfony/symfony/badge/rank.svg)](http://phppackages.org/p/symfony/symfony)
-->
<!--
  [![PHPPackages Referenced By](http://phppackages.org/p/symfony/symfony/badge/referenced-by.svg)](http://phppackages.org/p/symfony/symfony)
-->
* <!--- doctrine/orm -->
  <!--- 23 2019-11 -->
  ![Packagist Downloads](https://img.shields.io/packagist/dm/doctrine/orm)
  doctrine/orm
<!--
  [![PHPPackages Rank](http://phppackages.org/p/doctrine/orm/badge/rank.svg)](http://phppackages.org/p/doctrine/orm)
  [![PHPPackages Referenced By](http://phppackages.org/p/doctrine/orm/badge/referenced-by.svg)](http://phppackages.org/p/doctrine/orm)
-->
* <!--- symfony/framework-bundle -->
  <!--- 24 2019-11 -->
  ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/framework-bundle)
  symfony/framework-bundle
<!--
  [![PHPPackages Rank](http://phppackages.org/p/symfony/framework-bundle/badge/rank.svg)](http://phppackages.org/p/symfony/framework-bundle)
  [![PHPPackages Referenced By](http://phppackages.org/p/symfony/framework-bundle/badge/referenced-by.svg)](http://phppackages.org/p/symfony/framework-bundle)
-->
* <!--- psr/log -->
  <!--- 26 2019-10 -->
  ![Packagist Downloads](https://img.shields.io/packagist/dm/psr/log)
  [psr](https://phppackages.org/s/psr)/[log](https://gitlab.com/php-packages-demo/psr-log)
<!--
  [![PHPPackages Rank](http://phppackages.org/p/psr/log/badge/rank.svg)](http://phppackages.org/p/psr/log)
  [![PHPPackages Referenced By](http://phppackages.org/p/psr/log/badge/referenced-by.svg)](http://phppackages.org/p/psr/log)
-->
* <!--- twig/twig -->
  <!--- 27 2019-11 -->
  ![Packagist Downloads](https://img.shields.io/packagist/dm/twig/twig)
  twig/twig
<!--
  [![PHPPackages Rank](http://phppackages.org/p/twig/twig/badge/rank.svg)](http://phppackages.org/p/twig/twig)
  [![PHPPackages Referenced By](http://phppackages.org/p/twig/twig/badge/referenced-by.svg)](http://phppackages.org/p/twig/twig)
-->
* <!--- symfony/yaml -->
  <!--- 29 2019-11 -->
  ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/yaml)
  symfony/yaml
<!--
  [![PHPPackages Rank](http://phppackages.org/p/symfony/yaml/badge/rank.svg)](http://phppackages.org/p/symfony/yaml)
  [![PHPPackages Referenced By](http://phppackages.org/p/symfony/yaml/badge/referenced-by.svg)](http://phppackages.org/p/symfony/yaml)
-->
* <!--- phpstan/phpstan -->
  <!--- 34 2019-10 -->
  ![Packagist Downloads](https://img.shields.io/packagist/dm/phpstan/phpstan)
  [phpstan](https://phppackages.org/s/phpstan)/[phpstan](https://gitlab.com/php-packages-demo/phpstan)
<!--
  [![PHPPackages Rank](http://phppackages.org/p/phpstan/phpstan/badge/rank.svg)](http://phppackages.org/p/phpstan/phpstan)
  [![PHPPackages Referenced By](http://phppackages.org/p/phpstan/phpstan/badge/referenced-by.svg)](http://phppackages.org/p/phpstan/phpstan)
-->
* <!--- slim/slim -->
  <!--- 39 2019-11 -->
  ![Packagist Downloads](https://img.shields.io/packagist/dm/slim/slim)
  slim/slim
<!--
  [![PHPPackages Rank](http://phppackages.org/p/slim/slim/badge/rank.svg)](http://phppackages.org/p/slim/slim)
  [![PHPPackages Referenced By](http://phppackages.org/p/slim/slim/badge/referenced-by.svg)](http://phppackages.org/p/slim/slim)
-->
* <!--- symfony/http-foundation -->
  <!--- 45 2019-11 -->
  ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/http-foundation)
  symfony/http-foundation
<!--
  [![PHPPackages Rank](http://phppackages.org/p/symfony/http-foundation/badge/rank.svg)](http://phppackages.org/p/symfony/http-foundation)
  [![PHPPackages Referenced By](http://phppackages.org/p/symfony/http-foundation/badge/referenced-by.svg)](http://phppackages.org/p/symfony/http-foundation)
-->
* <!--- doctrine/dbal -->
  <!--- 47 2019-10 -->
  ![Packagist Downloads](https://img.shields.io/packagist/dm/doctrine/dbal)
  doctrine/[dbal](https://gitlab.com/php-packages-demo/doctrine-dbal)
<!--
  [![PHPPackages Rank](http://phppackages.org/p/doctrine/dbal/badge/rank.svg)](http://phppackages.org/p/doctrine/dbal)
  [![PHPPackages Referenced By](http://phppackages.org/p/doctrine/dbal/badge/referenced-by.svg)](http://phppackages.org/p/doctrine/dbal)
-->

* ![Packagist Downloads](https://img.shields.io/packagist/dm/makinacorpus/query-builder)
  makinacorpus/query-builder
  * ![Packagist Downloads](https://img.shields.io/packagist/dm/makinacorpus/db-tools-bundle)
    makinacorpus/db-tools-bundle
  * ![Packagist Downloads](https://img.shields.io/packagist/dm/makinacorpus/query-builder-bundle)
    makinacorpus/query-builder-bundle

* <!--- symfony/process -->
  <!--- 49 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/symfony/process/badge/rank.svg)](http://phppackages.org/p/symfony/process)
  [symfony](https://phppackages.org/s/symfony)/[process](https://gitlab.com/php-packages-demo/symfony-process)
  [![PHPPackages Referenced By](http://phppackages.org/p/symfony/process/badge/referenced-by.svg)](http://phppackages.org/p/symfony/process)
* <!--- symfony/http-kernel -->
  <!--- 50 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/symfony/http-kernel/badge/rank.svg)](http://phppackages.org/p/symfony/http-kernel)
  symfony/http-kernel
  [![PHPPackages Referenced By](http://phppackages.org/p/symfony/http-kernel/badge/referenced-by.svg)](http://phppackages.org/p/symfony/http-kernel)
* <!--- symfony/finder -->
  <!--- 53 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/symfony/finder/badge/rank.svg)](http://phppackages.org/p/symfony/finder)
  symfony/finder
  [![PHPPackages Referenced By](http://phppackages.org/p/symfony/finder/badge/referenced-by.svg)](http://phppackages.org/p/symfony/finder)
* <!--- symfony/event-dispatcher -->
  <!--- 63 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/symfony/event-dispatcher/badge/rank.svg)](http://phppackages.org/p/symfony/event-dispatcher)
  symfony/event-dispatcher
  [![PHPPackages Referenced By](http://phppackages.org/p/symfony/event-dispatcher/badge/referenced-by.svg)](http://phppackages.org/p/symfony/event-dispatcher)
* <!--- phalcon/cphalcon -->
  <!--- 66 2019-10 -->
  [![PHPPackages Rank](http://phppackages.org/p/phalcon/cphalcon/badge/rank.svg)](http://phppackages.org/p/phalcon/cphalcon)
  [phalcon](https://phppackages.org/s/phalcon)/[cphalcon](https://packagist.org/packages/phalcon/cphalcon)
  [![PHPPackages Referenced By](http://phppackages.org/p/phalcon/cphalcon/badge/referenced-by.svg)](http://phppackages.org/p/phalcon/cphalcon)
  * [apk-packages-demo](https://gitlab.com/apk-packages-demo/php-phalcon)
  * [apt-packages-demo](https://gitlab.com/apt-packages-demo/php-phalcon)
* <!--- symfony/dependency-injection -->
  <!--- 70 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/symfony/dependency-injection/badge/rank.svg)](http://phppackages.org/p/symfony/dependency-injection)
  [symfony](https://phppackages.org/s/symfony)/[dependency-injection](https://gitlab.com/php-packages-demo/symfony-dependency-injection)
  [![PHPPackages Referenced By](http://phppackages.org/p/symfony/dependency-injection/badge/referenced-by.svg)](http://phppackages.org/p/symfony/dependency-injection)
* <!--- psr/container -->
  <!--- 74 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/psr/container/badge/rank.svg)](http://phppackages.org/p/psr/container)
  psr/container
  [![PHPPackages Referenced By](http://phppackages.org/p/psr/container/badge/referenced-by.svg)](http://phppackages.org/p/psr/container)
* <!--- symfony/config -->
  <!--- 75 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/symfony/config/badge/rank.svg)](http://phppackages.org/p/symfony/config)
  symfony/config
  [![PHPPackages Referenced By](http://phppackages.org/p/symfony/config/badge/referenced-by.svg)](http://phppackages.org/p/symfony/config)
* <!--- symfony/css-selector -->
  <!--- 89 2018-10 -->
  [![PHPPackages Rank](http://phppackages.org/p/symfony/css-selector/badge/rank.svg)](http://phppackages.org/p/symfony/css-selector)
  [symfony](https://phppackages.org/s/symfony)/[css-selector](https://gitlab.com/php-packages-demo/symfony-css-selector)
  [![PHPPackages Referenced By](http://phppackages.org/p/symfony/css-selector/badge/referenced-by.svg)](http://phppackages.org/p/symfony/css-selector)
* <!--- doctrine/doctrine-bundle -->
  <!--- 92 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/doctrine/doctrine-bundle/badge/rank.svg)](http://phppackages.org/p/doctrine/doctrine-bundle)
  doctrine/doctrine-bundle
  [![PHPPackages Referenced By](http://phppackages.org/p/doctrine/doctrine-bundle/badge/referenced-by.svg)](http://phppackages.org/p/doctrine/doctrine-bundle)
* <!--- symfony/routing -->
  <!--- 99 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/symfony/routing/badge/rank.svg)](http://phppackages.org/p/symfony/routing)
  symfony/routing
  [![PHPPackages Referenced By](http://phppackages.org/p/symfony/routing/badge/referenced-by.svg)](http://phppackages.org/p/symfony/routing)
* <!--- league/oauth2-server -->
  <!--- 136 2018-10 -->
  [![PHPPackages Rank](http://phppackages.org/p/league/oauth2-server/badge/rank.svg)](http://phppackages.org/p/league/oauth2-server)
  [league](https://phppackages.org/s/league)/[oauth2-server](https://gitlab.com/php-packages-demo/league-oauth2-server)
  [![PHPPackages Referenced By](http://phppackages.org/p/league/oauth2-server/badge/referenced-by.svg)](http://phppackages.org/p/league/oauth2-server)
* <!--- sensio/framework-extra-bundle -->
  <!--- 137 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/sensio/framework-extra-bundle/badge/rank.svg)](http://phppackages.org/p/sensio/framework-extra-bundle)
  sensio/framework-extra-bundle
  [![PHPPackages Referenced By](http://phppackages.org/p/sensio/framework-extra-bundle/badge/referenced-by.svg)](http://phppackages.org/p/sensio/framework-extra-bundle)
* <!--- pimple/pimple -->
  <!--- 170 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/pimple/pimple/badge/rank.svg)](http://phppackages.org/p/pimple/pimple)
  pimple/pimple
  [![PHPPackages Referenced By](http://phppackages.org/p/pimple/pimple/badge/referenced-by.svg)](http://phppackages.org/p/pimple/pimple)
* <!--- robmorgan/phinx -->
  <!--- 186 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/robmorgan/phinx/badge/rank.svg)](http://phppackages.org/p/robmorgan/phinx)
  robmorgan/phinx
  [![PHPPackages Referenced By](http://phppackages.org/p/robmorgan/phinx/badge/referenced-by.svg)](http://phppackages.org/p/robmorgan/phinx)
  Database migrations
* <!--- symfony/browser-kit -->
  <!--- 215 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/symfony/browser-kit/badge/rank.svg)](http://phppackages.org/p/symfony/browser-kit)
  [symfony](https://phppackages.org/s/symfony)/[browser-kit](https://gitlab.com/php-packages-demo/browser-kit)
  [![PHPPackages Referenced By](http://phppackages.org/p/symfony/browser-kit/badge/referenced-by.svg)](http://phppackages.org/p/symfony/browser-kit)
* <!--- symfony/twig-bundle -->
  <!--- 216 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/symfony/twig-bundle/badge/rank.svg)](http://phppackages.org/p/symfony/twig-bundle)
  symfony/twig-bundle
  [![PHPPackages Referenced By](http://phppackages.org/p/symfony/twig-bundle/badge/referenced-by.svg)](http://phppackages.org/p/symfony/twig-bundle)
* <!--- facebook/webdriver -->
  <!--- 220 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/facebook/webdriver/badge/rank.svg)](http://phppackages.org/p/facebook/webdriver)
  [facebook](https://phppackages.org/s/facebook)/[webdriver](https://gitlab.com/php-packages-demo/facebook-webdriver)
  [![PHPPackages Referenced By](http://phppackages.org/p/facebook/webdriver/badge/referenced-by.svg)](http://phppackages.org/p/facebook/webdriver)
* <!--- doctrine/migrations -->
  <!--- 250 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/doctrine/migrations/badge/rank.svg)](http://phppackages.org/p/doctrine/migrations)
  [doctrine](https://phppackages.org/s/doctrine)/[migrations](https://gitlab.com/php-packages-demo/doctrine-migrations)
  [![PHPPackages Referenced By](http://phppackages.org/p/doctrine/migrations/badge/referenced-by.svg)](http://phppackages.org/p/doctrine/migrations)
* <!--- twig/extensions -->
  <!--- 258..259..261 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/twig/extensions/badge/rank.svg)](http://phppackages.org/p/twig/extensions)
  [twig](https://phppackages.org/s/twig)/[extensions](https://gitlab.com/php-packages-demo/twig-extensions)
  [![PHPPackages Referenced By](http://phppackages.org/p/twig/extensions/badge/referenced-by.svg)](http://phppackages.org/p/twig/extensions)
* <!--- phing/phing -->
  <!--- 276 2019-10 -->
  [![PHPPackages Rank](http://phppackages.org/p/phing/phing/badge/rank.svg)](http://phppackages.org/p/phing/phing)
  [phing](https://phppackages.org/s/phing)/[phing](https://gitlab.com/php-packages-demo/phing)
  [![PHPPackages Referenced By](http://phppackages.org/p/phing/phing/badge/referenced-by.svg)](http://phppackages.org/p/phing/phing)
* <!--- symfony/serializer -->
  <!--- 282 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/symfony/serializer/badge/rank.svg)](http://phppackages.org/p/symfony/serializer)
  [symfony](https://phppackages.org/s/symfony)/[serializer](https://gitlab.com/php-packages-demo/symfony-serializer)
  [![PHPPackages Referenced By](http://phppackages.org/p/symfony/serializer/badge/referenced-by.svg)](http://phppackages.org/p/symfony/serializer)
* <!--- symfony/security-bundle -->
  <!--- 284 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/symfony/security-bundle/badge/rank.svg)](http://phppackages.org/p/symfony/security-bundle)
  symfony/security-bundle
  [![PHPPackages Referenced By](http://phppackages.org/p/symfony/security-bundle/badge/referenced-by.svg)](http://phppackages.org/p/symfony/security-bundle)
* <!--- drush/drush -->
  <!--- 294 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/drush/drush/badge/rank.svg)](http://phppackages.org/p/drush/drush)
  [drush](https://phppackages.org/s/drush)/[drush](https://gitlab.com/php-packages-demo/drush)
  [![PHPPackages Referenced By](http://phppackages.org/p/drush/drush/badge/referenced-by.svg)](http://phppackages.org/p/drush/drush)
* <!--- symfony/expression-language -->
  <!--- 300 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/symfony/expression-language/badge/rank.svg)](http://phppackages.org/p/symfony/expression-language)
  symfony/expression-language
  [![PHPPackages Referenced By](http://phppackages.org/p/symfony/expression-language/badge/referenced-by.svg)](http://phppackages.org/p/symfony/expression-language)
* <!--- sonata-project/admin-bundle -->
  <!--- 301 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/sonata-project/admin-bundle/badge/rank.svg)](http://phppackages.org/p/sonata-project/admin-bundle)
  sonata-project/admin-bundle
  [![PHPPackages Referenced By](http://phppackages.org/p/sonata-project/admin-bundle/badge/referenced-by.svg)](http://phppackages.org/p/sonata-project/admin-bundle)
* <!--- phalcon/zephir -->
  <!--- 355 2019-10 -->
  [![PHPPackages Rank](http://phppackages.org/p/phalcon/zephir/badge/rank.svg)](http://phppackages.org/p/phalcon/zephir)
  phalcon/zephir
  [![PHPPackages Referenced By](http://phppackages.org/p/phalcon/zephir/badge/referenced-by.svg)](http://phppackages.org/p/phalcon/zephir)
* <!--- klein/klein -->
  <!--- 376 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/klein/klein/badge/rank.svg)](http://phppackages.org/p/klein/klein)
  klein/klein
  [![PHPPackages Referenced By](http://phppackages.org/p/klein/klein/badge/referenced-by.svg)](http://phppackages.org/p/klein/klein)
* <!--- zendframework/zend-eventmanager -->
  <!--- 405 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/zendframework/zend-eventmanager/badge/rank.svg)](http://phppackages.org/p/zendframework/zend-eventmanager)
  zendframework/zend-eventmanager
  [![PHPPackages Referenced By](http://phppackages.org/p/zendframework/zend-eventmanager/badge/referenced-by.svg)](http://phppackages.org/p/zendframework/zend-eventmanager)
* <!--- symfony/templating -->
  <!--- 424 2019-10 -->
  [![PHPPackages Rank](http://phppackages.org/p/symfony/templating/badge/rank.svg)](http://phppackages.org/p/symfony/templating)
  [symfony](https://phppackages.org/s/symfony)/[templating](https://gitlab.com/php-packages-demo/symfony-templating)
  [![PHPPackages Referenced By](http://phppackages.org/p/symfony/templating/badge/referenced-by.svg)](http://phppackages.org/p/symfony/templating)
* <!--- symfony/intl -->
  <!--- 461 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/symfony/intl/badge/rank.svg)](http://phppackages.org/p/symfony/intl)
  [symfony](https://phppackages.org/s/symfony)/[intl](https://gitlab.com/php-packages-demo/symfony-intl)
  [![PHPPackages Referenced By](http://phppackages.org/p/symfony/intl/badge/referenced-by.svg)](http://phppackages.org/p/symfony/intl)
* <!--- symfony/web-profiler-bundle -->
  <!--- 474 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/symfony/web-profiler-bundle/badge/rank.svg)](http://phppackages.org/p/symfony/web-profiler-bundle)
  symfony/web-profiler-bundle
  [![PHPPackages Referenced By](http://phppackages.org/p/symfony/web-profiler-bundle/badge/referenced-by.svg)](http://phppackages.org/p/symfony/web-profiler-bundle)
* <!--- easycorp/easyadmin-bundle -->
  <!--- 481 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/easycorp/easyadmin-bundle/badge/rank.svg)](http://phppackages.org/p/easycorp/easyadmin-bundle)
  easycorp/easyadmin-bundle
  [![PHPPackages Referenced By](http://phppackages.org/p/easycorp/easyadmin-bundle/badge/referenced-by.svg)](http://phppackages.org/p/easycorp/easyadmin-bundle)
* <!--- phalcon/devtools -->
  <!--- 552 2019-10 -->
  [![PHPPackages Rank](http://phppackages.org/p/phalcon/devtools/badge/rank.svg)](http://phppackages.org/p/phalcon/devtools)
  [phalcon](https://phppackages.org/s/phalcon)/[devtools](https://gitlab.com/php-packages-demo/phalcon-devtools)
  [![PHPPackages Referenced By](http://phppackages.org/p/phalcon/devtools/badge/referenced-by.svg)](http://phppackages.org/p/phalcon/devtools)
* <!--- evenement/evenement -->
  <!--- 545 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/evenement/evenement/badge/rank.svg)](http://phppackages.org/p/evenement/evenement)
  evenement/evenement
  [![PHPPackages Referenced By](http://phppackages.org/p/evenement/evenement/badge/referenced-by.svg)](http://phppackages.org/p/evenement/evenement)
* <!--- symfony/orm-pack -->
  <!--- 645 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/symfony/orm-pack/badge/rank.svg)](http://phppackages.org/p/symfony/orm-pack)
  symfony/orm-pack
  [![PHPPackages Referenced By](http://phppackages.org/p/symfony/orm-pack/badge/referenced-by.svg)](http://phppackages.org/p/symfony/orm-pack)
* <!--- symfony/web-server-bundle -->
  <!--- 746 2019-10 -->
  [![PHPPackages Rank](http://phppackages.org/p/symfony/web-server-bundle/badge/rank.svg)](http://phppackages.org/p/symfony/web-server-bundle)
  [symfony](https://phppackages.org/s/symfony)/[web-server-bundle](https://gitlab.com/php-packages-demo/symfony-web-server-bundle)
  [![PHPPackages Referenced By](http://phppackages.org/p/symfony/web-server-bundle/badge/referenced-by.svg)](http://phppackages.org/p/symfony/web-server-bundle)
* <!--- white-october/pagerfanta-bundle -->
  <!--- 777 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/white-october/pagerfanta-bundle/badge/rank.svg)](http://phppackages.org/p/white-october/pagerfanta-bundle)
  white-october/pagerfanta-bundle
  [![PHPPackages Referenced By](http://phppackages.org/p/white-october/pagerfanta-bundle/badge/referenced-by.svg)](http://phppackages.org/p/white-october/pagerfanta-bundle)
* <!--- knplabs/doctrine-behaviors -->
  <!--- 803 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/knplabs/doctrine-behaviors/badge/rank.svg)](http://phppackages.org/p/knplabs/doctrine-behaviors)
  [knplabs](https://phppackages.org/s/knplabs)/[doctrine-behaviors](https://gitlab.com/php-packages-demo/knplabs-doctrine-behaviors)
  [![PHPPackages Referenced By](http://phppackages.org/p/knplabs/doctrine-behaviors/badge/referenced-by.svg)](http://phppackages.org/p/knplabs/doctrine-behaviors)
* <!--- symfony/panther -->
  <!--- 853 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/symfony/panther/badge/rank.svg)](http://phppackages.org/p/symfony/panther)
  [symfony](https://phppackages.org/s/symfony)/[panther](https://gitlab.com/php-packages-demo/symfony-panther)
  [![PHPPackages Referenced By](http://phppackages.org/p/symfony/panther/badge/referenced-by.svg)](http://phppackages.org/p/symfony/panther)
* <!--- symfony/profiler-pack -->
  <!--- 853..854 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/symfony/profiler-pack/badge/rank.svg)](http://phppackages.org/p/symfony/profiler-pack)
  symfony/profiler-pack
  [![PHPPackages Referenced By](http://phppackages.org/p/symfony/profiler-pack/badge/referenced-by.svg)](http://phppackages.org/p/symfony/profiler-pack)
* <!--- phpbench/phpbench -->
  <!--- 837 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/phpbench/phpbench/badge/rank.svg)](http://phppackages.org/p/phpbench/phpbench)
  phpbench/phpbench
  [![PHPPackages Referenced By](http://phppackages.org/p/phpbench/phpbench/badge/referenced-by.svg)](http://phppackages.org/p/phpbench/phpbench)
* <!--- symfony-cmf/routing -->
  <!--- 1213 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/symfony-cmf/routing/badge/rank.svg)](http://phppackages.org/p/symfony-cmf/routing)
  symfony-cmf/routing
  [![PHPPackages Referenced By](http://phppackages.org/p/symfony-cmf/routing/badge/referenced-by.svg)](http://phppackages.org/p/symfony-cmf/routing)
* <!--- symfony/proxy-manager-bridge -->
  <!--- 1668 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/symfony/proxy-manager-bridge/badge/rank.svg)](http://phppackages.org/p/symfony/proxy-manager-bridge)
  symfony/proxy-manager-bridge
  [![PHPPackages Referenced By](http://phppackages.org/p/symfony/proxy-manager-bridge/badge/referenced-by.svg)](http://phppackages.org/p/symfony/proxy-manager-bridge)
* <!--- greenlion/php-sql-parser -->
  <!--- 1768 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/greenlion/php-sql-parser/badge/rank.svg)](http://phppackages.org/p/greenlion/php-sql-parser)
  [greenlion](https://phppackages.org/s/greenlion)/[php-sql-parser](https://gitlab.com/php-packages-demo/greenlion-php-sql-parser)
  [![PHPPackages Referenced By](http://phppackages.org/p/greenlion/php-sql-parser/badge/referenced-by.svg)](http://phppackages.org/p/greenlion/php-sql-parser)
* <!--- symfony-cmf/routing-bundle -->
  <!--- 1777 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/symfony-cmf/routing-bundle/badge/rank.svg)](http://phppackages.org/p/symfony-cmf/routing-bundle)
  symfony-cmf/routing-bundle
  [![PHPPackages Referenced By](http://phppackages.org/p/symfony-cmf/routing-bundle/badge/referenced-by.svg)](http://phppackages.org/p/symfony-cmf/routing-bundle)
* <!--- symfony-cmf/routing-extra-bundle -->
  <!--- 1675 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/symfony-cmf/routing-extra-bundle/badge/rank.svg)](http://phppackages.org/p/symfony-cmf/routing-extra-bundle)
  symfony-cmf/routing-extra-bundle
  [![PHPPackages Referenced By](http://phppackages.org/p/symfony-cmf/routing-extra-bundle/badge/referenced-by.svg)](http://phppackages.org/p/symfony-cmf/routing-extra-bundle)
* <!--- networking/init-cms-bundle -->
  <!--- 4773 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/networking/init-cms-bundle/badge/rank.svg)](http://phppackages.org/p/networking/init-cms-bundle)
  networking/init-cms-bundle
  [![PHPPackages Referenced By](http://phppackages.org/p/networking/init-cms-bundle/badge/referenced-by.svg)](http://phppackages.org/p/networking/init-cms-bundle)
* <!--- symfony-cmf/routing-auto-bundle -->
  <!--- 5008 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/symfony-cmf/routing-auto-bundle/badge/rank.svg)](http://phppackages.org/p/symfony-cmf/routing-auto-bundle)
  symfony-cmf/routing-auto-bundle
  [![PHPPackages Referenced By](http://phppackages.org/p/symfony-cmf/routing-auto-bundle/badge/referenced-by.svg)](http://phppackages.org/p/symfony-cmf/routing-auto-bundle)
* <!--- symfony-cmf/routing-auto -->
  <!--- 10487 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/symfony-cmf/routing-auto/badge/rank.svg)](http://phppackages.org/p/symfony-cmf/routing-auto)
  symfony-cmf/routing-auto
  [![PHPPackages Referenced By](http://phppackages.org/p/symfony-cmf/routing-auto/badge/referenced-by.svg)](http://phppackages.org/p/symfony-cmf/routing-auto)
* <!--- sylius/mailer-bundle -->
  <!--- 12693 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/sylius/mailer-bundle/badge/rank.svg)](http://phppackages.org/p/sylius/mailer-bundle)
  [sylius](https://phppackages.org/s/sylius)[mailer-bundle](https://gitlab.com/php-packages-demo/sylius-mailer-bundle)
  [![PHPPackages Referenced By](http://phppackages.org/p/sylius/mailer-bundle/badge/referenced-by.svg)](http://phppackages.org/p/sylius/mailer-bundle)
* <!--- symfony/service-contracts -->
  <!--- 14071 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/symfony/service-contracts/badge/rank.svg)](http://phppackages.org/p/symfony/service-contracts)
  symfony/service-contracts
  [![PHPPackages Referenced By](http://phppackages.org/p/symfony/service-contracts/badge/referenced-by.svg)](http://phppackages.org/p/symfony/service-contracts)
* <!--- symfony/mailer -->
  <!--- 44529 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/symfony/mailer/badge/rank.svg)](http://phppackages.org/p/symfony/mailer)
  [symfony](https://phppackages.org/s/symfony)/[mailer](https://gitlab.com/php-packages-demo/symfony-mailer)
  [![PHPPackages Referenced By](http://phppackages.org/p/symfony/mailer/badge/referenced-by.svg)](http://phppackages.org/p/symfony/mailer)
* <!--- olvlvl/symfony-dependency-injection-proxy -->
  <!--- 52875 2019-11   71230 2018-10 -->
  [![PHPPackages Rank](http://phppackages.org/p/olvlvl/symfony-dependency-injection-proxy/badge/rank.svg)](http://phppackages.org/p/olvlvl/symfony-dependency-injection-proxy)
  [olvlvl](https://phppackages.org/s/olvlvl)/[symfony-dependency-injection-proxy](https://gitlab.com/php-packages-demo/olvlvl-symfony-dependency-injection-proxy)
  [![PHPPackages Referenced By](http://phppackages.org/p/olvlvl/symfony-dependency-injection-proxy/badge/referenced-by.svg)](http://phppackages.org/p/olvlvl/symfony-dependency-injection-proxy)
* <!--- symfony/notifier -->
  <!--- 61699 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/symfony/notifier/badge/rank.svg)](http://phppackages.org/p/symfony/notifier)
  [symfony](https://phppackages.org/s/symfony)/[notifier](https://gitlab.com/php-packages-demo/symfony-notifier)
  [![PHPPackages Referenced By](http://phppackages.org/p/symfony/notifier/badge/referenced-by.svg)](http://phppackages.org/p/symfony/notifier)

## Some projects in this group by referenced by
* <!--- 4597 2018-10 -->
  [![PHPPackages Referenced By](http://phppackages.org/p/symfony/symfony/badge/referenced-by.svg)](http://phppackages.org/p/symfony/symfony)
  [symfony](https://phppackages.org/s/symfony)/[symfony](https://gitlab.com/php-packages-demo/symfony)
  [![PHPPackages Rank](http://phppackages.org/p/symfony/symfony/badge/rank.svg)](http://phppackages.org/p/symfony/symfony)
* <!--- 1598 2018-10-->
  [![PHPPackages Referenced By](http://phppackages.org/p/symfony/css-selector/badge/referenced-by.svg)](http://phppackages.org/p/symfony/css-selector)
  [symfony](https://phppackages.org/s/symfony)/[css-selector](https://gitlab.com/php-packages-demo/symfony-css-selector)
  [![PHPPackages Rank](http://phppackages.org/p/symfony/css-selector/badge/rank.svg)](http://phppackages.org/p/symfony/css-selector)
* <!--- 132 2018-10-->
  [![PHPPackages Referenced By](http://phppackages.org/p/league/oauth2-server/badge/referenced-by.svg)](http://phppackages.org/p/league/oauth2-server)
  [league](https://phppackages.org/s/league)/[oauth2-server](https://gitlab.com/php-packages-demo/league-oauth2-server)
  [![PHPPackages Rank](http://phppackages.org/p/league/oauth2-server/badge/rank.svg)](http://phppackages.org/p/league/oauth2-server)
* <!--- 0 2018-10 -->
  [![PHPPackages Referenced By](http://phppackages.org/p/olvlvl/symfony-dependency-injection-proxy/badge/referenced-by.svg)](http://phppackages.org/p/olvlvl/symfony-dependency-injection-proxy)
  [olvlvl](https://phppackages.org/s/olvlvl)/[symfony-dependency-injection-proxy](https://gitlab.com/php-packages-demo/olvlvl-symfony-dependency-injection-proxy)
  [![PHPPackages Rank](http://phppackages.org/p/olvlvl/symfony-dependency-injection-proxy/badge/rank.svg)](http://phppackages.org/p/olvlvl/symfony-dependency-injection-proxy)

## PHP Packages translated from other languages
### [symfony](https://phppackages.org/s/symfony)/[css-selector](https://gitlab.com/php-packages-demo/symfony-css-selector)
#### Python
* [cssselect](https://pypi.org/project/cssselect/)

## Unofficial documentation about PHP
* [*5 Common Mistakes PHP Developers Make when Writing SQL*](https://www.eversql.com/5-common-mistakes-php-developers-make-when-writing-sql/)
